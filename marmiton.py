#!/usr/bin/python
# -*- coding: utf-8 -*-

import urllib, sys



def recup_page(url):
    f = urllib.urlopen(url)
    a = f.read()
    f.close()
    return a



def transforme(a):
    titre = a.split('class="fn">')[1].split("</span>")[0].replace("\t","").replace("  "," ").replace("  "," ")
    
    recette = a.split('class="m_content_recette_main">')[1].split('</div>')[0]
    
    x = recette.split('<p class="m_content_recette_info">')[1].split('</p>')[0]
    
    
    tps_prep = x.split('<span class="preptime">')[1].split('<span')[0].replace("<br>","").replace(" : ","").replace("min","minutes")
    tps_cuiss = x.split('<span class="cooktime">')[1].split('<span')[0].replace("<br>","").replace(" : ","").replace("min","minutes")
    
    quantity = recette.split('<span>Ingrédients (pour')[1].split(') :</span>')[0]

    classifications= a.split('m_content_recette_breadcrumb">')[1].split('</div>')[0]
    listofclass=classifications.split('-')
    cat=listofclass[0]
    difficulty=listofclass[1]
    cost=listofclass[2]
  
    liste_ingredients = recette.split('<span>Ingrédients (pour')[1].split(') :</span>')[1].split('</p>')[0].replace("<br/>-","\n").replace("-","\n").replace("<br/><br/>","\n")
    
    liste_ingredients = liste_ingredients.replace("\n\n","").replace("<br>","").replace("<br/>","").replace("<br />","")
    while(liste_ingredients.find('aspx')!=-1):
	stringtoreplace='<a'+liste_ingredients.split('<a')[1].split('">')[0]+'">'
	liste_ingredients=liste_ingredients.replace(stringtoreplace,"")
	

    liste_ingredients = liste_ingredients.replace("</a>","").replace("<br>","").lstrip()
    
    
    preparation = recette.split('<h4>Préparation de la recette :</h4><br/>')[1].split('<div class="m_content_recette_ps">')[0].replace("<br>"," ")
    

    preparation = preparation.replace("<br/><br/>","\n")
    
    while(preparation.find('aspx')!=-1):
	stringtoreplace1='<a'+preparation.split('<a')[1].split('">')[0]+'">'
	preparation=preparation.replace(stringtoreplace1,"")
        
	    
    preparation = preparation.replace("</a>","")
    preparation = preparation.replace("\n\n","").lstrip()

    if (a.find('m_content_recette_illu"')==-1):
	photo="Pas de photo disponible"
    else:
        photocontent=a.split('m_content_recette_illu"')[1].split('</a>')[0]
        photo=photocontent.split('class="photo" src=')[1].split('alt')[0].replace("'","")

    ps=a.split('m_content_recette_ps">')[1].split('</div>')[0]

    if (ps.find('Conseil vin :')==-1):
	wine="Pas de vin conseillé disponible"
    else:
        wine=ps.split('Conseil vin :</h4>')[1].split('</p>')[0].replace("<p>","").lstrip()

    if (ps.find('Remarques :')==-1):
	remarque="Pas de remarque supplémentaire"
    else:
	remarque=ps.split('Remarques :</h4>')[1].split('</p>')[0].replace("<p>","").lstrip()
        
    
    
    finish = "Recette: " + titre + "\n"
    finish += "\n"
    finish += "Categorie: " + cat + "\n"
    finish += "Cout: " + cost + "\n"
    finish += "Difficulté: " + difficulty + "\n"
    finish += "\n"
    finish += "Temps de préparation: " + tps_prep + " minutes\n"
    finish += "Temps de cuisson: " + tps_cuiss + " minutes\n"
    finish += "\n"
    finish += "Ingrédients pour" + quantity +":\n"
    finish += "\n"
    finish += liste_ingredients
    finish += "\n"
    finish += "Préparation: \n"
    finish += "\n"
    finish += preparation
    finish += "\n"
    finish += "Remarque: "+remarque
    finish += "\n"
    finish += "Vin conseillé: "+wine
    finish += "\n"
    finish += "URL photo: " +photo

    name = titre.replace(" ","_")+'.txt'  # Name of text file coerced with +.txt

    try:
        file = open(name,'w')   # Trying to create a new file or open one
	file.write(finish)
        file.close()

    except:
        print('La recette n´a pas été sauvegardé')
        sys.exit(0) # quit Python
    

transforme(recup_page(sys.argv[1]))

