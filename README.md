**Read-me**

Le script marmiton.py permet d´extraire les informations importantes d´une recette sur le site Marmiton. 
Pour utiliser ce script, il suffit de taper : python marmiton.py *url_de_votre_recette*  .  
Un fichier texte contenant la recette va alors être créé dans le même dossier que le script. Ce fichier texte a le même nom que le nom de la recette. 

Ce code a pour seul but une utilisation personnelle et ne doit pas être utilisé pour fin commercial.